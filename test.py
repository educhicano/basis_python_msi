# -*- coding: utf-8 -*-

# Test workflow for MSI processing

import sys
import os 
from basis.utils.cmdline import OptionsHolder;
from basis.io.manageh5db import save_data2matlab

# Package upload
module_path = os.path.abspath('%s'%os.path.dirname(os.path.realpath('test.py')));
sys.path.append(module_path);
sys.path.insert(0,module_path)



datafolder = '/Users/kirillveselkov/Desktop/DESI_art/'
dbimport   = '/Users/kirillveselkov/Desktop/test/import01.h5'
datapath   = '/Users/kirillveselkov/Desktop/test/Negdesi'
h5rawdbname   = '/Users/kirillveselkov/Desktop/test/Negdesi/pyimport2.h5'
h5dbname = '/Users/kirillveselkov/Desktop/test/Negdesi/pyproc6.h5'

datafolder = '//Users/kirillveselkov/Dropbox/Github/pymspackages/pyproc2/Data'
h5dbname = '/Users/kirillveselkov/Desktop/test/Negdesi/pyprocEmm.h5'

#==============================================================================
# from basis.preproc.palign import PeakAlign
# from basis.io.manageh5db import save_preproc_obj,load_preproc_obj
# p = PeakAlign(method='great',params={'sad': [1,3,4,], 'dada': 'string'})
# load_preproc_obj(h5dbname,'Peak Alignment',pathinh5 ='ddd/')
#     
#==============================================================================

# Import Module 
#==============================================================================
# import basis.io.importmsi as imp
# from basis.procconfig import ImportSet_options
# settings=OptionsHolder(__doc__, ImportSet_options)
# settings.parameters['h5rawdbname'] = dbimport
# 
# settings.description='Import Raw Data';
# print(settings.program_description);
# print('\nStarting.....')
# settings.parameters['datapath'] = datafolder
# 
# settings.parameters['filereadinfo']['delimiter'] = '\t'
#==============================================================================

#imp.main(settings.parameters['datapath'], settings.parameters['filetype'], settings.parameters['filereadinfo'], settings.parameters['h5rawdbname']);


# Peak alignment module
#==============================================================================
# from basis.procconfig import PeakAlign_options;
# import basis.preproc.palign as pa
# settings=OptionsHolder(__doc__, PeakAlign_options);
# settings.description='Peak Alignment';
# settings.do='yes';
# 
# settings.parameters['h5rawdbname'] = h5rawdbname
# settings.parameters['h5dbname'] = h5dbname
# 
# pa.do_alignment(settings.parameters['h5rawdbname'], \
#                  h5dbname = settings.parameters['h5dbname'], \
#                  method = settings.parameters['method']);
#==============================================================================

#==============================================================================
# # Intra-sample normalization
# from basis.procconfig import IntraNorm_options
# import basis.preproc.intranorm as intranorm
# settings=OptionsHolder(__doc__, IntraNorm_options);
# settings.description='Itranormalization Settings';
# settings.do='yes';
# settings.parameters['h5dbname'] = dbproc 
# 
# intranorm.do_normalize(h5dbname=settings.parameters['h5dbname'],\
#              method=settings.parameters['method'],\
#              params = settings.parameters['params'],\
#              mzrange=[settings.parameters['min_mz'], settings.parameters['max_mz']]);
# 
# 
# # Inter-sample normalization
# from basis.procconfig import InterNorm_options
# import basis.preproc.internorm as internorm
# settings=OptionsHolder(__doc__, InterNorm_options);   
# settings.description='Iternormalization Settings';
# settings.do='yes';
# settings.gscale=[];
# settings.parameters['h5dbname'] = dbproc 
# 
# internorm.do_normalize(h5dbname=settings.parameters['h5dbname'],\
#              method=settings.parameters['method'],\
#              params = settings.parameters['params'],\
#              mzrange=[settings.parameters['min_mz'], settings.parameters['max_mz']])
#              
#              
# # Variance stabilizing transformation
# from basis.procconfig import VST_options
# import basis.preproc.vst as vst
# 
# settings=OptionsHolder(__doc__, VST_options);
# settings.description='Variance Stabilizing Transformation Settings';
# settings.do='yes';
# settings.parameters['h5dbname'] = dbproc
# 
# vst.do_vst(dbprocpath=settings.parameters['h5dbname'], method=settings.parameters['method'], params=settings.parameters['params']);
# 
# # Peak filtering
# from basis.procconfig import PeakFilter_options
# import basis.preproc.pfilter as pf
# 
# settings=OptionsHolder(__doc__, PeakFilter_options);   
# settings.description='Peak Filtering Settings';
# settings.do='yes';
#==============================================================================

   # settings.parameters['h5dbname'] = '/Users/kirillveselkov/desktop/DESI_Art/pyproc_data__1928_22_03_2017.h5'
#==============================================================================
# settings.parameters['h5dbname'] = dbproc
# pf.do_filtering(dbprocpath=settings.parameters['h5dbname'],\
#              method=settings.parameters['method'],\
#              params = settings.parameters['params'])
#==============================================================================
# # 
# #==============================================================================
# print('\nFinished.');
# print(settings.description_epilog);
#==============================================================================
# #==============================================================================
# 
# save_data2matlab(dbproc)
#==============================================================================
import basis.io.exportmsi as ex 
from basis.procconfig import ExportSet_options;
settings=OptionsHolder(__doc__, ExportSet_options);
settings.description='Export MSI Data';
settings.parameters['h5dbname'] = h5dbname
#settings.parameters['datapath'] = '/Users/kv/Desktop/test'
ex.do_export(settings.parameters['h5dbname'], settings.parameters['filetype'], settings.parameters['params']);
print('\nFinished.');
print(settings.description_epilog);

