# BASIS_pyproc #

# Python processing pipeline for MSI data #

Brief Summary
=============

The package includes a set of modules for optimized analytical 
pre-processing workflow for raw MSI data. It accounts for common 
bio-analytical complexities (including data volume burden, 
platform-specific biases and heteroscedastic noise structure) 
inherent to high-throughput MALDI- and DESI-MSI datasets of 
tissue specimens. All pre-processing modules have been designed to 
operate in an iterative fashion (i.e. processing a single dataset at a time) 
and are thus scalable for processing of hundreds to thousands of specimens.

This workflow is an essential prerequisite 
for processing and integrating heterogeneous 
MSI datasets for downstream machine learning approaches.

The modules include:  

    *  **HDF5-based Database Management** to organise, retrieve and store large volumes of numerical MS imaging datasets

    *  **Peak Alignment** to account for non-linear drifts of mass to charge or collision cross section measurements of a given molecule between multiple datasets                   
        
    *  **Inter-sample normalization** to adjust for spectrum-to-spectrum differences in overall scale within individual datasets.  
    
    *  **Intra-sample normalization** to bring MSI datasets of multiple samples into a common scale 

    *  **Variance-stabilizing transformation** to account for increased technical variability as a function of increased intensity

    *  **Cluster-driven peak filtering** to remove solvent or matrix related molecular ion features


Setup
-----

1. This pipeline uses python for data processing. We strongly recommend installing one of the versions of [Anaconda](https://www.anaconda.com/download/) python from: https://www.anaconda.com/download/ as  it comes with many dependencies already pre-installed.

2. Download and uncompress (using gunzip) and untar the release to a local folder on your PC or Mac. 
 
3. Install dependencies. This pipeline requires the following libraries for data processing: NumPy, SciPy, Pandas, Scikit-learn, h5py, pyimzml. You will also benefit greatly if you run the pipeline via Jupyter-Notebook. Anaconda comes with a number of pre-requisities already satisfied. To install the remaining pre-requisities you may want to cd to the folder containing your local copy of BASIS_pyproc and run the following command in your console terminal: `pip install -r requirements.txt`   

4. Either run commands individually via command line (not recommended) or open Jupyter notebook provided along with this package (MSIworkflow-HDI.ipynb or MSIworkflow-imzML.ipynb) and follow its  instructions. Both versions of the notebook are interchangeable and are just pre-configured for HDI and imzML formats for TOF and Orbitrap instruments respectively for your convenience. 

 __Note__: Currently Windows users can experience problems with automatic installation of pyimzml due to issues with installing wheezy.template-0.1.167.tar.gz. Please refer to the following online forum   advice how to install wheezy.template manually: [https://stackoverflow.com/questions/48659073/pip-stuck-while-using-cached-wheezy-template-0-1-167-tar-gz/48662424#48662424] (https://stackoverflow.com/questions/48659073/pip-stuck-while-using-cached-wheezy-template-0-1-167-tar-gz/48662424#48662424). After it is installed, installation of the rest of the modules should proceed  as normal. Mac and Linux users seem to be unaffected by this issue at the moment. 


Examples
--------

The following on-line folder contains some sample MSI data which can be used to test the pipeline:

[http://www.mediafire.com/folder/4u7oo2hxfoktl/BASIS\_TestData](http://www.mediafire.com/folder/4u7oo2hxfoktl/BASIS_TestData/)

You would need to download the archive and unpack the files in some local folder on your hard drive.
We strongly advise you to use the Jupyter notebook provided and follow its instructions for the general use (MSIworkflow.ipynb).



For detailed information please refer to the help for specific modules (also available via Jupyter notebook provided):

```
cd <your local BASIS_pyproc folder>
```

Data Import: 

```
python ./basis/io/importmsi.py --help
```

Peak Alignment: 

```
python ./basis/preproc/palign.py --help
```

Intra-sample normalization:  

```
python ./basis/preproc/intranorm.py --help
```

Inter-sample normalization:  

```
python ./basis/preproc/internorm.py --help
```

Variance Stabilizing Transformation:  

```
python ./basis/preproc/vst.py --help
```

Matrix peak filtering:

```
python ./basis/preproc/pfilter.py --help
```

Results Export:

```
python ./basis/io/exportmsi.py --help
```





Authors
-------

Chief project investigator: Dr. Kirill Veselkov (kirill.veselkov04@imperial.ac.uk)


Acknowledgments
---------------
Any papers describing data analytics using this package, or whose results were significantly 
aided by the use of this package (except when the use was internal to a larger program), 
should include an acknowledgment and citation to the following manuscript(s): 


If the package is used prior to its open source release and publication, 
please contact kirill.veselkov04@imperial.ac.uk) 
to ensure that all contributors are fairly acknowledged.  

Issues
------

Please report any bugs or requests that you have using the Bitbucket issue tracker!


Development
-----------

To be added...




Licenses
-----------

The code which makes up this Python project template is licensed under the BSD license (see License.txt). 
Feel free to use it in your free software/open-source or proprietary projects.

The template also uses a number of other pieces of software, whose licenses are listed here for convenience. It is your responsibility to ensure that these licenses are up-to-date for the version of each tool you are using.

+------------------------+----------------------------------+
|Project                 |License                           |
+========================+==================================+
|Python itself           |Python Software Foundation License|
+------------------------+----------------------------------+
|Sphinx                  |Simplified BSD License            |
+------------------------+----------------------------------+
|numpy                   |BSD License                       |
+------------------------+----------------------------------+
|scipy                   |BSD License                       |
+------------------------+----------------------------------+
|h5py                    |BSD License                       |
+------------------------+----------------------------------+
|scipy                   |BSD License                       |
+------------------------+----------------------------------+
